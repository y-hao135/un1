package config;

import java.io.FileWriter;
import java.io.IOException;

/* *
 *类名：AlipayConfig
 *功能：基础配置类
 *详细：设置帐户有关信息及返回路径
 *修改日期：2017-04-05
 *说明：
 *以下代码只是为了方便商户测试而提供的样例代码，商户可以根据自己网站的需要，按照技术文档编写,并非一定要使用该代码。
 *该代码仅供学习和研究支付宝接口使用，只是提供一个参考。
 */

public class AlipayConfig {

//↓↓↓↓↓↓↓↓↓↓请在这里配置您的基本信息↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓

    // 应用ID,您的APPID，收款账号既是您的APPID对应支付宝账号
    public static String app_id = "2021000118674680";

    // 商户私钥，您的PKCS8格式RSA2私钥
    public static String merchant_private_key = "MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCLZR1Amf6OqMBTLjYMPS2Bmy20OQ7E+y6sKe7o7s92s9e2eB68WeAKQvSKOjnTfiEW/shaypwfjYntxLMyFlNHCnr7r1fu2PNr8OfYkWEKLLIApn6PORJa+4CROSwfTHBDpUvrjjTV7oeF3XZgA6aj0OMTlixgbDEcCkSJwgJz8kzMPKYULKDFOkQr73E/PKch9fdyRcVyspPtzgsZ93x2ffzEGNjt6CBPu3Gv9mZgXzBugp51xUQC3Irrpg3d/ehljyskg18uAO2YTEh72q15weGdWBIqJxyzgDdqu2/+e+B6DKHzxgowr8blOsNT0UwqvYQPzrQ3nDCtHDFY8/sdAgMBAAECggEAZuGgL3f5C229VSc8ONhGZ+wSVXvvTK9fCS6B6GWptaCSy1fox3dGBgzY09SZrWDgJ0qwsWHszNT4Qgb1gzg7CqnPj0chDWYOc1KAq7TQ/GN0lUpNCoRC6efKQmUCDa+rBCJR4G86y9KbL8/+eGr5aNrZXjFuW1Q/AsUJROtuSAgpK42GFrDBG3AVuLFX38AdJlrQGf5azNuLR7Nfp3U3Qwtb03sAZsfMiQlvikxpDlLtUlHPTo4SX1J8ti1S0wsutElCjVIvGQMMMFyuJNbQSJAuTBw7PrXmrObeF2fJIDZmnQ2WmKKmbP0qSb/gKQHqfA+GprNYwJQ1LBDSE/B/+QKBgQDcxz/CJk2/iwHqJiuFjelWg8LQccdD2sTLcI77b34g9Gl5IvvdaSZrnoXzMOQEOXfjd9M6To4T2Ynyo3k6wVHo9Wiv9FxUB5VdF9+Q07Gs0vlHIz5m9pRrAkJWRpnDx88CseBKS/8MAyV+w3/hHZ4YVZF8VSdDKLpUePtCxo/NZwKBgQChohvoERqXXR+WR3t+nkhwdJRaSEebmuwvIEjl7LCOB4BvWr/2JHv/BCSmdNjN1Hz8bP/EAnpjSmARyNNtrF08UM7f7FR+X3On2v8d6/v3u0jQ3Iya2mX2/8kOrTrk5YsNoRz8PdMg20O5udYu2oL+1VdfTw6LC31uKOXdz8gc2wKBgAvMId4QCeuIVAvdCkpSsPtMgyZ8TfeUQxvT4fARnx3HsbMlaWwFaGYrzGCYZjbLk1p2VrUw6WqqHzL2BU5ccjIOmw978UsUNKkK04l7pmzjwSpJ+dWqq3ekYUMW9rR9gtRUCPYvZWuQ53oEnFq9KGuIdwO70SNHk91bsaF51slVAoGAEPNoS9EnYcJhYqhCekftPuXq/QDhiZ8Yr+SJ4JsO9QYyYPs6cHzpCBiOI2rTpdpn9S0VMaA0557QLm1KRxExgfbTi0ZEojAud+cOpDuwZA2fGFBY6pCG6v1uP3zJVWIBUD+aQ5XUQ3jbRGoJ3UYLQOBs1lD7LHfbAqeFW9rDmMsCgYEAmg1MBwgNkhNTLiuiE1auW1MirQUyTCHcAY0y0zvE8tGrGDsOxZoK3Op+DdxStwzlFYzK7y/dzs1D4lxQebQn/WtV6EOLiFf5iLxTg6VycnAaPlKjCgOi2iWNYF4JEv3yHTrWKgJ5zSe9lIBKShuTSWmmw/T+qiMSDcW4uKLex7E=";

    // 支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm 对应APPID下的支付宝公钥。
    public static String alipay_public_key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAqgUN2j7Dds+YIvcl8MaQNtkNbzrJA+tJ5i0Y3FaHBSwSNlXUWQg+Wq6Xs7zsSWjoSk0eLSuOb5s8kL579AbN4oDN8MGCtZBKS8X8ZniVeihsix5B0U+w7zt/Ifw9cf2JvffxWHRVDG3KJ1ZsqsQLA3OrvGiChpSMPZ6p0mlKYXUFJAxVRF8a4bBjN5TmEb4OetOdu45hDxRVNhWzq25KCCPHJwTTEZQjuC50aGnck0enuxCDio4s0YNdXFfxWuU6xxRlef1lpHJJmuB0e4iEnUysZMiMxHJHWEO9frERrjUsGOm/lgcGAtVWUuN/+nzEIGl5HdovyGSstnuNpEmiXQIDAQAB";

    // 服务器异步通知页面路径  需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    public static String notify_url = "http://localhost/8080/notify_url.jsp";

    // 页面跳转同步通知页面路径 需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    public static String return_url = "http://localhost/8080/return_url.jsp";

    // 签名方式
    public static String sign_type = "RSA2";

    // 字符编码格式
    public static String charset = "utf-8";

    // 支付宝网关
    public static String gatewayUrl = "https://openapi.alipaydev.com/gateway.do";

    // 支付宝网关
    public static String log_path = "C:\\";


//↑↑↑↑↑↑↑↑↑↑请在这里配置您的基本信息↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑

    /**
     * 写日志，方便测试（看网站需求，也可以改成把记录存入数据库）
     * @param sWord 要写入日志里的文本内容
     */
    public static void logResult(String sWord) {
        FileWriter writer = null;
        try {
            writer = new FileWriter(log_path + "alipay_log_" + System.currentTimeMillis()+".txt");
            writer.write(sWord);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}

